class CellError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class Cell:
    DEAD = "DEAD"
    ALIVE = "ALIVE"

    def __init__(self, neighbours):
        if len(neighbours) > 8:
            raise CellError('Too many neighbours')
        self.__neighbours = neighbours
        self.__state = Cell.DEAD

    def add_neighbour(self, neighbour):
        if len(self.__neighbours) == 8:
            raise CellError('Already have eight neighbours')
        self.__neighbours.append(neighbour)

    @property
    def state(self):
        return self.__state

    @property
    def neighbours(self):
        return self.__neighbours

    def __str__(self):
        state = "alive" if self.__state == Cell.ALIVE else "dead"
        return 'A cell which is {}'.format(state)

    def make_dead(self):
        self.__state = Cell.DEAD

    def make_alive(self):
        self.__state = Cell.ALIVE

    def change_state(self):
        def count_alive_neighbours(neighbours):
            count = 0
            # NB not using 'for cell in neighbours' to introduce bug
            for x in range(len(neighbours) - 1):
                if neighbours[x].__state == Cell.ALIVE:
                    count += 1
            return count

        living = count_alive_neighbours(self.__neighbours)
        if self.__state == Cell.DEAD:
            if living == 3:
                self.make_alive()
        elif living > 3 or living < 2:
            self.make_dead()
