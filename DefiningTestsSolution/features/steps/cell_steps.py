from behave import *
from features.cell.cell import *


@given('a new cell')
def g1(context):
    context.the_cell = Cell([])


@given('a new cell with neighbours')
def g2(context):
    context.neighbours = [Cell([]) for x in range(0, 8)]
    context.the_cell = Cell(context.neighbours)


@then('the state of the cell is {state}')
def t1(context, state):
    assert context.the_cell.state == state


@then('the next state will be {state}')
def t3(context, state):
    context.the_cell.change_state()
    assert context.the_cell.state == state


@when('we make it ALIVE')
def w1(context):
    context.the_cell.make_alive()


@when('we make it DEAD')
def w2(context):
    context.the_cell.make_dead()


@when('we add a new neighbour')
def w3(context):
    try:
        context.the_cell.add_neighbour(Cell([]))
    except CellError as ex:
        context.current_exception = ex


@when('{num:d} neighbours are ALIVE')
def w4(context, num):
    neighbours = context.the_cell.neighbours
    for x in range(num):
        neighbours[x].make_alive()
