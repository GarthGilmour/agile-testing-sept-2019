from behave import *
from features.arithmetic.math import *


@given('we have a math object holding {no1:f} and {no2:f}')
def g1(context, no1, no2):
    context.math_obj = Math(no1, no2)


@given('the values and operations below')
def g2(context):
    context.stuff = context.table


@when('we choose the {name:w} operation')
def w1(context, name):
    math = context.math_obj

    try:
        if name == 'add':
            result = math.add()
        elif name == 'subtract':
            result = math.subtract()
        elif name == 'multiply':
            result = math.multiply()
        elif name == 'divide':
            result = math.divide()
        else:
            assert False, "Unknown operation specified"
    except MathError as ex:
        context.current_exception = ex
    else:
        context.result = result


@then('the result is {result:f}')
def t1(context, result):
    assert context.result == result


@then('all is well')
def t2(context):
    for row in context.stuff:
        context.math_obj = Math(float(row['no1']), float(row['no2']))
        context.result = float(row['result'])
        context.execute_steps('''
            When we choose the %s operation
            Then the result is %s
        ''' % (row['operation'], row['result']))
