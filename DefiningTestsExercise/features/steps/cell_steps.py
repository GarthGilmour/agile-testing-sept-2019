from behave import *
from features.cell.cell import *


@given('a new cell')
def g1(context):
    context.the_cell = Cell([])


@then('the state of the cell is {state}')
def t1(context, state):
    assert context.the_cell.state == state
