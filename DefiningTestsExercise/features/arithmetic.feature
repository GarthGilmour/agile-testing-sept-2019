Feature: Testing the Math type

  Scenario: we can add values
    Given we have a math object holding 12.0 and 13.0
    When we choose the add operation
    Then the result is 25.0

  Scenario: we can subtract values
    Given we have a math object holding 35.0 and 12.0
    When we choose the subtract operation
    Then the result is 23.0

  Scenario: we can multiply values
    Given we have a math object holding 5.0 and 6.0
    When we choose the multiply operation
    Then the result is 30.0

  Scenario: we can divide values
    Given we have a math object holding 30.0 and 6.0
    When we choose the divide operation
    Then the result is 5.0

  Scenario: dividing by zero throws an exception
    Given we have a math object holding 5.0 and 0.0
    When we choose the divide operation
    Then an exception is thrown

  Scenario: everything works
    Given the values and operations below
      | no1  | no2  | operation | result |
      | 12.0 | 34.0 | add       | 46.0   |
      | 56.0 | 34.0 | subtract  | 22.0   |
      | 12.0 | 4.0  | multiply  | 48.0   |
      | 12.0 | 2.0  | divide    | 6.0    |
    Then all is well
