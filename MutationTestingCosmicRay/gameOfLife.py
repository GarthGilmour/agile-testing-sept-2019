class Cell:
    DEAD = 0
    ALIVE = 1

    def __init__(self, neighbours):
        if len(neighbours) > 8:
            raise RuntimeError('Too many neighbours')
        self.__neighbours = neighbours
        self.__state = Cell.DEAD

    def state(self):
        return self.__state

    def __str__(self):
        state = "alive" if self.__state == Cell.ALIVE else "dead"
        return 'A cell which is {}'.format(state)

    def make_dead(self):
        self.__state = Cell.DEAD

    def make_alive(self):
        self.__state = Cell.ALIVE

    def change_state(self):
        def count_alive_neighbours(neighbours):
            count = 0
            for cell in neighbours:
                if cell.state() == cell.ALIVE:
                    count += 1
            return count

        living = count_alive_neighbours(self.__neighbours)
        if self.state() == Cell.DEAD:
            if living == 3:
                self.make_alive()
        elif living > 3 or living < 2:
            self.make_dead()
