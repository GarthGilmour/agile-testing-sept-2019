import unittest

from gameOfLife import Cell


class CellTest(unittest.TestCase):
    def setUp(self):
        self.__neighbours = [
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([])
        ]
        self.cell = Cell(self.__neighbours)

    def test_is_dead_by_default(self):
        self.assertEqual(Cell.DEAD, self.cell.state())

    def test_can_be_made_alive(self):
        self.cell = Cell(self.__neighbours)

        self.cell.make_alive()

        self.assertEqual(Cell.ALIVE, self.cell.state())

    def test_can_be_made_dead(self):
        self.cell.make_alive()

        self.cell.make_dead()

        self.assertEqual(Cell.DEAD, self.cell.state())

    def test_throws_exception_if_created_with_more_than_8_neighbours(self):
        self.__neighbours.append(Cell([]))

        self.assertRaises(RuntimeError, lambda: Cell(self.__neighbours))

    def test_will_become_alive_through_reporoduction(self):
        self.__neighbours[2].make_alive()
        self.__neighbours[5].make_alive()
        self.__neighbours[6].make_alive()

        self.cell.change_state()

        self.assertEqual(Cell.ALIVE, self.cell.state())

    # Uncomment the test below to see surviving tests getting killed
    # def test_str_works(self):
    #     self.assertEquals("A cell which is dead", str(self.cell))
    #
    #     self.cell.make_alive()
    #     self.assertEquals("A cell which is alive", str(self.cell))


if __name__ == '__main__':
    unittest.main()
