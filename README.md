# Testing in an Agile World

These are the examples and exercises for the **Testing in an Agile World** course delivering at Instil on 26th September 2019

The accompanying manual [can be downloaded from here](https://drive.google.com/open?id=1pD21hLaSKyNZXbyNEzqin5OGH7VHvy_W).


